//
// Copyright (C) 2013~2017 by CSSlayer
// wengxt@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef _GUI_ADDSHORTCUTDIALOG_H_
#define _GUI_ADDSHORTCUTDIALOG_H_

#include "shortcutmodel.h"
#include "ui_addshortcutdialog.h"
#include <QDialog>
#include <QMap>

namespace fcitx {

class AddShortcutDialog : public QDialog, public Ui::AddShortcutDialog {
    Q_OBJECT
public:
    explicit AddShortcutDialog(QWidget *parent = 0);
    virtual ~AddShortcutDialog();

    bool valid();
    ShortcutEntry shortcut();
public Q_SLOTS:
    void keyChanged();

private:
    int length_;
    gchar **commands_;
};
} // namespace fcitx

#endif // _GUI_ADDSHORTCUTDIALOG_H_
